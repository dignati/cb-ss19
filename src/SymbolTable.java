import java.util.HashMap;

class Signature {
    int nParams;
    Signature(int nParams) {
        this.nParams = nParams;
    }
}

public class SymbolTable {
    private int varCounter = 0; 
    private HashMap<String, String> constants;
    private HashMap<String, Integer> variables;
    private HashMap<String, Signature> procs;
    private HashMap<String, Signature> funcs;

    SymbolTable() {
        this.constants = new HashMap<>();
        this.variables = new HashMap<>();
        this.procs = new HashMap<>();
        this.funcs = new HashMap<>();
    }

    boolean hasVariable(String id) {
      return variables.containsKey(id);
    }

    boolean hasConstant(String id) {
      return constants.containsKey(id);
    }
    
    boolean hasSymbol(String id) {
      return hasVariable(id) || hasConstant(id);
    }

    void addConstant(String id, String val) throws SymbolAlreadyDefinedException {
        if (hasSymbol(id)) {
            throw new SymbolAlreadyDefinedException();
        }
        constants.put(id, val);
    }

    int addVariable(String id) throws SymbolAlreadyDefinedException {
      if(hasSymbol(id)) {
        throw new SymbolAlreadyDefinedException();
      }
      variables.put(id, varCounter++);
      return varCounter - 1;
    }

    String getConstant(String id) throws UnknownSymbolException {
        if (hasConstant(id)) {
            return constants.get(id);
        }
        throw new UnknownSymbolException();
    }

    int getVariable(String id) throws UnknownSymbolException {
        if (hasVariable(id)) {
            return variables.get(id);
        }
        throw new UnknownSymbolException();
    }

    public void addProc(String symbol, int nParams) {
        this.procs.put(symbol, new Signature(nParams));
    }

    public Signature getSignature(String symbol) {
        Signature s = this.funcs.get(symbol);
        if (s == null) {
            return this.procs.get(symbol);
        } else {
            return s;
        }
    }

    public void addFunc(String symbol, int nParams) {
        this.funcs.put(symbol, new Signature(nParams));
    }
}
