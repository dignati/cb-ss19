import java.util.Queue;
import java.util.LinkedList;

interface RealInstr {
    byte[] toBytes();
}

interface PseudoInstr {
    String toString();
    int bytes();
}

class BiPush implements RealInstr {
    private Integer lit;

    BiPush(Integer lit) {
        this.lit = lit;
    }

    public byte[] toBytes() {
        return new byte[]{0x10, lit.byteValue()};
    }
}

class Multiply implements RealInstr {
    public byte[] toBytes() {
        return new byte[]{0x68};
    }
}

class Division implements RealInstr {
    public byte[] toBytes() {
        return new byte[]{0x62};
    }
}

class Addition implements RealInstr {
    public byte[] toBytes() {
        return new byte[]{0x60};
    }
}

class Subtraction implements RealInstr {
    public byte[] toBytes() {
        return new byte[]{0x64};
    }
}

class Comp implements RealInstr {
    private String op;
    private RelJump relJump;

    public Comp(String op, RelJump relJump) {
        this.op = op;
        this.relJump = relJump;
    }

    public byte[] toBytes() {
        byte[] code = new byte[3];
        switch (op) {
            case "!=":
                code[0] = (byte) 0x9f;
                break;
            case "==":
                code[0] = (byte) 0xa0;
                break;
            case ">=":
                code[0] = (byte) 0xa1;
                break;
            case "<=":
                code[0] = (byte) 0xa3;
                break;
            case ">":
                code[0] = (byte) 0xa4;
                break;
            case "<":
                code[0] = (byte) 0xa2;
                break;
        }
        int offset = relJump.getOffset();
        code[1] = (byte) (offset >> 8);
        code[2] = (byte) (offset);
        return code;
    }
}

class Store implements RealInstr {
    private int loc;

    Store(int loc) {
        this.loc = loc;
    }

    public byte[] toBytes() {
        return new byte[]{0x36, (byte) loc};
    }
}

class StaticStore implements PseudoInstr {
    private String ident;

    StaticStore(String ident) {
        this.ident = ident;
    }

    public String toString() {
        return String.format("b3 [%s]", ident);
    }

    public int bytes() {
        return 3;
    }
}

class Load implements RealInstr {
    private int loc;

    Load(int loc) {
        this.loc = loc;
    }

    public byte[] toBytes() {
        return new byte[]{0x15, (byte) loc};
    }
}

class StaticLoad implements PseudoInstr {
    private String ident;
    StaticLoad(String ident) {
        this.ident = ident;
    }
    public String toString() {
        return String.format("b2 [%s]", ident);
    }

    public int bytes() {
        return 3;
    }
}

class Jump implements RealInstr {
    private RelJump relJump;

    Jump(RelJump relJump) {
        this.relJump = relJump;
    }

    public byte[] toBytes() {
        byte[] code = new byte[3];
        code[0] = (byte) 0xa7;
        int offset = relJump.getOffset();
        code[1] = (byte) (offset >> 8);
        code[2] = (byte) (offset);
        return code;
    }
}

class Ret implements RealInstr {
    public byte[] toBytes() {
        return new byte[]{(byte) 0xb1};
    }
}

class IRet implements  RealInstr {
    public byte[] toBytes() {
        return new byte[]{(byte) 0xac};
    }
}

class Print implements PseudoInstr {
    public String toString() {
        return "b8 (print)";
    }

    public int bytes() {
        return 3;
    }
}

class Call implements PseudoInstr {
    private String ident;
    public Call(String ident) {
        this.ident = ident;
    }
    public String toString() {
        return String.format("b8 (%s)", ident);
    }

    public int bytes() {
        return 3;
    }
}

public class CodeGen {

    private Queue<Object> instr;
    private SymbolTable globals;
    private SymbolTable locals;
    private SymbolTable params;
    private int instrCounter;
    private String methodName;
    public int nParams;
    private boolean isMain;

    public CodeGen(String methodName) {
        this.instr = new LinkedList<>();
        this.globals = new SymbolTable();
        this.locals = new SymbolTable();
        this.params = new SymbolTable();
        this.instrCounter = 0;
        this.methodName = methodName;
        this.isMain = true;
    }

    public CodeGen fork(String methodName) {
        CodeGen gen = new CodeGen(methodName);
        gen.globals = this.locals;
        gen.isMain = false;
        return gen;
    }

    private void add(RealInstr i) {
        this.instrCounter += i.toBytes().length;
        instr.add(i);
    }

    private void add(PseudoInstr i) {
        this.instrCounter += i.bytes();
        instr.add(i);
    }

    public MethodObject generate() {
        StringBuilder s = new StringBuilder();
        for (Object i : instr) {
            if (i instanceof RealInstr) {
                for (byte b : ((RealInstr) i).toBytes()) {
                    s.append(String.format("%02x", (byte) b));
                }
            } else if (((PseudoInstr) i) instanceof PseudoInstr) {
                s.append(i.toString());
            }
            s.append(" ");
        }
        return new MethodObject(methodName, nParams, s.toString());
    }

    public void literal(String lit) {
        add(new BiPush(Integer.valueOf(lit)));
    }

    private void load(int loc) {
        add(new Load(loc));
    }

    private void staticLoad(String ident) {
        add(new StaticLoad(ident));
    }

    private void store(int loc) {
        add(new Store(loc));
    }

    private void staticStore(String ident) {
        add(new StaticStore(ident));
    }

    public void variable(String symbol) {
        if (locals.hasVariable(symbol)) {
            if(isMain){
                staticLoad(symbol);
            } else {
                load(locals.getVariable(symbol));
            }
        } else if (locals.hasConstant(symbol)) {
            literal(locals.getConstant(symbol));
        } else if (globals.hasConstant(symbol)) {
            literal(globals.getConstant(symbol));
        } else {
            staticLoad(symbol);
        }
    }

    public void declareConst(String symbol, String value) {
        locals.addConstant(symbol, value);
    }

    public void declareVar(String symbol, String value) {
        if (isMain) {
            locals.addVariable(symbol);
            literal(value);
            staticStore(symbol);
        } else {
            int loc = locals.addVariable(symbol);
            literal(value);
            store(loc);
        }
    }

    public void declareProc(String symbol, int nParams) {
        this.locals.addProc(symbol, nParams);
    }

    public void declareFunc(String symbol, int nParams) {
        this.locals.addFunc(symbol, nParams);
    }

    public void declareParam(String symbol) {
        this.nParams++;
        this.locals.addVariable(symbol);
    }

    public void assignVar(String symbol) {
        try {
            if (locals.hasVariable(symbol)) {
                int loc = locals.getVariable(symbol);
                store(loc);
            } else if (globals.hasVariable(symbol)) {
                int loc = globals.getVariable(symbol);
                staticStore(symbol);
            }
        } catch (UnknownSymbolException e) {
            try {
                locals.getConstant(symbol);
                throw new AssignToConstantException();
            } catch (UnknownSymbolException ex) {
                throw e;
            }
        }
    }

    public void mul() {
        add(new Multiply());
    }

    public void div() {
        add(new Division());
    }

    public void add() {
        add(new Addition());
    }

    public void sub() {
        add(new Subtraction());
    }

    public RelJump comp(String op) {
        RelJump relJump = this.beginBranch();
        add(new Comp(op, relJump));
        return relJump;
    }

    public void print() {
        add(new Print());
    }

    public void call(String i, int nParams) {
        Signature s = this.globals.getSignature(i);
        if (s == null) {
            s = this.locals.getSignature(i);
        }
        if (s == null) {
            throw new RValueException();
        }
        if (s.nParams != nParams) {
            throw new WrongParametersException();
        }
        add(new Call(i));
    }

    public void jump(RelJump relJump) {
        add(new Jump(relJump));
    }

    public RelJump jump() {
        RelJump relJump = RelJump.withStart(this.instrCounter);
        add(new Jump(relJump));
        return relJump;
    }

    public void ret() {
        add(new Ret());
    }

    public void iret() {
        add(new IRet());
    }

    public RelJump beginBranch() {
        return RelJump.withStart(this.instrCounter);
    }

    public void endBranch(RelJump relJump) {
        relJump.setTarget(this.instrCounter);
    }

    public RelJump beginLoop() {
        return RelJump.withTarget(this.instrCounter);
    }

    public void endLoop(RelJump relJump) {
        relJump.setStart(this.instrCounter);
        jump(relJump);
    }
}
