public class RelJump {
    private int start;
    private int target;

    public RelJump() {
        start = 0;
        target = 0;
    }

    public static RelJump withStart(int start) {
        RelJump l = new RelJump();
        l.start = start;
        return l;
    }

    public static RelJump withTarget(int target) {
        RelJump l = new RelJump();
        l.target = target;
        return l;
    }

    public void setTarget(int target) {
        this.target = target;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getOffset() {
        return this.target - this.start;
    }
}