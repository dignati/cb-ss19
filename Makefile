expr: gen/ExprParse.class
	java -cp src/:gen/ src/ExprParse

gen/ExprParse.class: gen/ExprParse.java
	javac -cp src/:gen/ gen/ExprParse.java

gen/ExprParse.java: parser/expr.jj
	javacc parser/expr.jj

miniE: gen/MiniJavaExprParser.class
	java -cp src/:gen/ MiniJavaExprParser

gen/MiniJavaExprParser.class: gen/MiniJavaExprParser.java
	javac -d target/ -cp src/:gen/ gen/MiniJavaExprParser.java

gen/MiniJavaExprParser.java: parser/miniJavaExpr.jj src/*.java
	javacc parser/miniJavaExpr.jj

mini: gen/MiniJavaParser.class
	java -cp target/ MiniJavaParser
	java Test

gen/MiniJavaParser.class: gen/MiniJavaParser.java src/*.java
	javac -d target/ -cp src/:gen/ gen/MiniJavaParser.java

gen/MiniJavaParser.java: parser/miniJava.jj
	javacc parser/miniJava.jj


fun: gen/MiniJavaFunProcParser.class
	java -cp target/ MiniJavaFunProcParser
	java Test

gen/MiniJavaFunProcParser.class: gen/MiniJavaFunProcParser.java src/*.java
	javac -d target/ -cp src/:gen/ gen/MiniJavaFunProcParser.java

gen/MiniJavaFunProcParser.java: parser/miniJavaFunProc.jj
	javacc parser/miniJavaFunProc.jj