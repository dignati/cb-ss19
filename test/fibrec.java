final int a = 1;
int calls = 0;

void log() {
    calls = calls + 1;
}

func fib(int n) {
    final int b = 2;
    int r = 1;
    {
        print(b+r);
        log();
        if n > 2 {
            a = 1;
            r = (fib(n-a) + fib(n-b));
        }
    }
    return r;

}

{
print(fib(12));
print(calls);
}
